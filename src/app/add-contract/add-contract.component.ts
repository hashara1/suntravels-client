import { Component, OnInit } from '@angular/core';
import {FormArray, FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import {Hotel} from './model/hotel';
import {ApiService} from '../shared/api.service';
import {AddContractViewModel} from './model/add-contract-view-model';
import {MatDialog} from '@angular/material/dialog';
import {DialogComponent} from '../shared/dialog/dialog.component';

@Component({
  selector: 'app-add-contract',
  templateUrl: './add-contract.component.html',
  styleUrls: ['./add-contract.component.css']
})
export class AddContractComponent implements OnInit {

   model: AddContractViewModel = {
     startDate: new Date(),
     endDate: new Date(),
     markup: 0,
     hotel: {
       hotelId: "00000000-0000-0000-0000-000000000000",
       type: '',
       name: '',
       location: '',
       telephone: '',
       email: '',
     },
     roomTypes: []
   };
  hotels: Hotel[];
  selectedHotel: Hotel;
  show: boolean = true;
  addContractForm: FormGroup;
  initialId:string;
  submitted:boolean;
  booleanArray: boolean[] = [];
  constructor(private apiService: ApiService, private fb: FormBuilder,public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllHotels();
    this.booleanArray = [];
    this.initialId = "00000000-0000-0000-0000-000000000000";
    this.submitted = false;
    this.addContractForm = this.fb.group({
      startDate: new Date(),
      endDate: new Date(),
      markup: ['', Validators.required],
      hotel: this.fb.group({
        hotelId: "00000000-0000-0000-0000-000000000000",
        type: ['', Validators.required],
        name: ['', Validators.required],
        location: ['', Validators.required],
        telephone: ['', Validators.required],
        email: ['', Validators.required]
      }),
      roomTypes: this.fb.array([])
    });
    this.addContractForm.get("hotel").get("hotelId").valueChanges.subscribe(value => {
      const type = this.addContractForm.get("hotel").get("type");
      const name = this.addContractForm.get("hotel").get("name");
      const location = this.addContractForm.get('hotel').get('location');
      const telephone = this.addContractForm.get('hotel').get('telephone');
      const email = this.addContractForm.get('hotel').get('email');

      if(value == this.initialId){
        this.show = true;
        type.setValidators(Validators.required);
        name.setValidators(Validators.required);
        location.setValidators(Validators.required);
        telephone.setValidators(Validators.required);
        email.setValidators(Validators.required);
      }
      else {
        this.show = false;
        this.selectedHotel = this.hotels.find(hotel => hotel.hotelId === value);
        type.clearValidators();
        name.clearValidators();
        location.clearValidators();
        telephone.clearValidators();
        email.clearValidators();
      }
      type.updateValueAndValidity();
      name.updateValueAndValidity();
      location.updateValueAndValidity();
      telephone.updateValueAndValidity();
      email.updateValueAndValidity();
    });
  }

  get roomTypeForms(): FormArray {
    return this.addContractForm.get('roomTypes') as FormArray;
  }

  addRoomType(): void{
    const roomType = this.fb.group({
      processingOrder: '',
      roomTypeId: this.initialId,
      type: [],
      maxAdults: [],
      noRooms: ['', Validators.required],
      price: ['', Validators.required],
    });
    const roomTypesArray = this.addContractForm.get('roomTypes') as FormArray;

    roomType.get("processingOrder").patchValue(roomTypesArray.length );

    this.booleanArray[roomTypesArray.length] = true;

    roomType.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.roomTypeForms.push(roomType);

  }

  onValueChanged(data?: any): void {
    const groupIndex = data["processingOrder"];

    console.log(groupIndex);
    const myChangedGroup = (this.addContractForm.get("roomTypes") as FormArray).controls[groupIndex];

    if(data['roomTypeId'] == this.initialId){
      this.booleanArray[groupIndex] = true;
    }
    else {
      this.booleanArray[groupIndex] = false;
    }
  }

  deleteRoomType(i): void {
    this.roomTypeForms.removeAt(i);
  }

  addContract(): void{
    this.submitted = true;
    this.apiService.postContract(this.addContractForm.value).subscribe(
      res => {
        this.openDialog();
      },
      error => {
        alert('Error has occurred');
      }
    );
  }

  findSelectedHotel(): void{
    if(this.model.hotel.hotelId == "0"){
      this.model.hotel.hotelId = "00000000-0000-0000-0000-000000000000";
      this.show = true;
    }
    else {
      this.show = false;
      this.selectedHotel = this.hotels.find(hotel => hotel.hotelId === this.model.hotel.hotelId);
    }
  }

  public getAllHotels(): void {
    this.apiService.getAllHotels().subscribe(
      res => {
        this.hotels = res;
      },
      error => {
        alert('Error has occurred');
      }
    );
  }

  openDialog(): void {
    this.dialog.open(DialogComponent, {
      data: {}
    });
  }

  get hotelType(){
    return this.addContractForm.get('hotel').get('type');
  }

  get hotelName(){
    return this.addContractForm.get('hotel').get('name');
  }

  get hotelTele(){
    return this.addContractForm.get('hotel').get('telephone');
  }

  get hotelEmail(){
    return this.addContractForm.get('hotel').get('email');
  }

  get hotelLocation(){
    return this.addContractForm.get('hotel').get('location');
  }


}


