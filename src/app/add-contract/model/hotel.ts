import {RoomType} from './room-type';

export class Hotel {
  hotelId: string;
  type: string;
  name: string;
  location: string;
  telephone: string;
  email: string;
  roomTypes: RoomType[];
}
