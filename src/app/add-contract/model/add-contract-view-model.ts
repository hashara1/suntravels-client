import {RoomType} from '../../view-contracts/model/room-type';

export class AddContractViewModel {
  startDate: Date;
  endDate: Date;
  markup: number;
  hotel: {
    hotelId: string;
    type: string;
    name: string;
    location: string;
    telephone: string;
    email: string;
  };
  roomTypes: RoomType[];
}

