import { Injectable } from '@angular/core';
import {HttpClient, HttpHandler, HttpHeaders} from '@angular/common/http';
import {Contract} from '../view-contracts/model/contract';
import {Observable} from 'rxjs';
import {Hotel} from '../add-contract/model/hotel';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private BASE_URL = `http://localhost:8080/api`;

  constructor(private http: HttpClient) { }

  getAllContracts(): Observable<Contract[]> {
    const url = `${this.BASE_URL}/contract/all`;
    return this.http.get<Contract[]>(url);
  }

  getAllHotels(): Observable<Hotel[]> {
    const url = `${this.BASE_URL}/hotel/all`;
    return this.http.get<Hotel[]>(url);
  }

  postContract(data: any): any{
    const url = `${this.BASE_URL}/contract/add`;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this.http.post(url,data,
      {headers:headers});
  }

  postSearchResult(data: any): any{
    const url = `${this.BASE_URL}/hotel/room`;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this.http.post(url,data,
      {headers:headers});
  }


}
