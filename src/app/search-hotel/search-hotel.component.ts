import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-hotel',
  templateUrl: './search-hotel.component.html',
  styleUrls: ['./search-hotel.component.css']
})
export class SearchHotelComponent implements OnInit {

  searchForm: FormGroup;

  constructor(private apiService: ApiService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    localStorage.clear();
    this.searchForm = this.fb.group({
      checkingDate: new Date(),
      numberOfNights: 1,
      roomsAdults: this.fb.array([])
    });

  }

  get roomsAdultsForm(): FormArray {
    return this.searchForm.get('roomsAdults') as FormArray;
  }

  addRoomsAdults(): void{
    const roomAdults = this.fb.group({
      numberOfAdults: 1
    });

    this.roomsAdultsForm.push(roomAdults);
  }

  deleteRoomsAdults(i): void {
    this.roomsAdultsForm.removeAt(i);
  }

  searchHotel(): void  {
    localStorage.setItem('searchInput', JSON.stringify(this.searchForm.value));
    this.apiService.postSearchResult(this.searchForm.value).subscribe(
      res => {
          localStorage.setItem('searchResult', JSON.stringify(res));
          this.router.navigate(['search']);
      },
      error => {
        alert('Error has occurred');
      }
    );

  }

  get checkingDate(): any{
    return this.searchForm.get('checkingDate');
  }

  get numberOfNights(): any{
    return this.searchForm.get('numberOfNights');
  }
}
