import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddContractComponent} from './add-contract/add-contract.component';
import {ViewContractsComponent} from './view-contracts/view-contracts.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {SearchHotelComponent} from './search-hotel/search-hotel.component';
import {SearchResultsComponent} from './search-results/search-results.component';
import {RoomSelectionComponent} from './search-results/component/room-selection/room-selection.component';

const routes: Routes = [
  {
    path: 'contract/add',
    component: AddContractComponent
  },
  {
    path: 'contract/view-all',
    component: ViewContractsComponent
  },
  {
    path: '',
    component: SearchHotelComponent
  },
  {
    path: 'search',
    component: SearchResultsComponent
  },
  {
    path: 'search/rooms',
    component: RoomSelectionComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
