import { Component, OnInit } from '@angular/core';
import {Contract} from './model/contract';
import {ApiService} from '../shared/api.service';


@Component({
  selector: 'app-view-contracts',
  templateUrl: './view-contracts.component.html',
  styleUrls: ['./view-contracts.component.css']
})

export class ViewContractsComponent implements OnInit {

  contracts: Contract[];
  searchText;

  constructor(private apiService: ApiService) {
    // this.usersUrl =
  }

  ngOnInit(): void {
    this.getAllContracts();
  }

  public getAllContracts(): void {
    this.apiService.getAllContracts().subscribe(
      res => {
        this.contracts = res;
      },
      error => {
        alert('Error has occurred');
      }
    );
  }
}
