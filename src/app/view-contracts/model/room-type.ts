export class RoomType {
  roomTypeId: string;
  type: string;
  maxAdults: number;
  noRooms: number;
  price: number;
}
