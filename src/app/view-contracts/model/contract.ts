import {RoomType} from './room-type';

export interface Contract {
  startDate: Date;
  endDate: Date;
  markup: number;
  contractId: string;
  hotel: {
    hotelId: string;
    type: string;
    name: string;
    location: string;
    telephone: string;
    email: string;
  };
  roomTypes: RoomType[];

}



