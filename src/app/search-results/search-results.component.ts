import { Component, OnInit } from '@angular/core';
import {SearchResult} from './model/search-result';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  searchResults: SearchResult[];

  constructor() { }

  ngOnInit(): void {

    this.searchResults =  JSON.parse(localStorage.getItem('searchResult'));
  }


}
