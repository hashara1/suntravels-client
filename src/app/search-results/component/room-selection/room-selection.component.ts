import { Component, OnInit } from '@angular/core';
import {SearchResult} from '../../model/search-result';
import {SearchInput} from './model/search-input';

@Component({
  selector: 'app-room-selection',
  templateUrl: './room-selection.component.html',
  styleUrls: ['./room-selection.component.css']
})
export class RoomSelectionComponent implements OnInit {

  hotel: SearchResult;
  searchInput: SearchInput;
  selectedIndex = 0;

  constructor() { }

  ngOnInit(): void {
    this.searchInput =  JSON.parse(localStorage.getItem('searchInput'));
    this.hotel =  JSON.parse(localStorage.getItem('selectedHotel'));

  }

  selectTab(index: number): void {
    this.selectedIndex = index;
  }

}
