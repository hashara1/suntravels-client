import {RoomAdult} from './room-adult';

export class SearchInput {
  checkingDate: Date;
  numberOfNights: number;
  roomsAdults: RoomAdult[];
}
