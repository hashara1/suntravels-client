import { Component, OnInit, Input  } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  @Input() parentData;

  constructor(private router: Router) { }

  ngOnInit(): void {
    localStorage.removeItem('selectedHotel');
  }

  onClick(): void{
    localStorage.setItem('selectedHotel', JSON.stringify(this.parentData));
    this.router.navigate(['search/rooms']);
  }

}
