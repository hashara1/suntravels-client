export class Hotel {
  hotelId: string;
  type: string;
  name: string;
  location: string;
  telephone: string;
  email: string;
}
