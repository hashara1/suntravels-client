export class ContractsRoomTypesKey {
  contractId: string;
  roomTypeId: string;
}
