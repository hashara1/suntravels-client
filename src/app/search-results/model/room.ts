import {ContractsRoomTypesKey} from './contracts-room-types-key';

export class Room {
  contractsRoomTypesKey: ContractsRoomTypesKey;
  type: string;
  maxAdults: number;
  price: number;
  noRooms: number;
}
