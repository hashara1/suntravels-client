import {Hotel} from './hotel';
import {Room} from './room';

export class SearchResult {
  hotel: Hotel;
  rooms: Room[];
  possible: boolean;
}
